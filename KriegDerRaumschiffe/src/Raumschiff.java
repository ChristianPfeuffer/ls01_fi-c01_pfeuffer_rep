import java.util.ArrayList;
import java.lang.Math;
import java.util.Iterator;

/**
 * Diese Klasse modelliert ein Raumschiff
 * 
 * @version 1.0
 * @author Christian Pfeuffer
 */
public class Raumschiff {
	private int photonentorpedoAnzahl;
	private int energieversorgungInProzent;
	private int schildeInProzent;
	private int huelleInProzent;
	private int lebenserhaltungssystemeInProzent;
	private String schiffsname;
	private int androidenAnzahl;
	private static ArrayList<String> broadcastKommunikator = new ArrayList<String>();
	private ArrayList<Ladung> ladungsverzeichnis = new ArrayList<Ladung>();

	/**
	 * Konstruktor fuer die Klasse Raumschiff
	 */
	public Raumschiff() {
	}

	/**
	 * Vollparametrisierter Konstruktor fuer die Klasse Raumschiff
	 * 
	 * @param Die Anzahl der Photonentorpedos im Raumschiff
	 * @param Die vorhandene Energieversorgung des Raumschiffs in Prozent
	 * @param Die vorhandenen Schilde des Raumschiffs in Prozent
	 * @param Die vorhandene Huelle des Raumschiffs in Prozent
	 * @param Die vorhandenen Lebenserhaltungssysteme des Raumschiffs in Prozent
	 * @param Der Name des Raumschiffs
	 * @param Die vorhandenen Androiden im Raumschiff
	 */
	public Raumschiff(int photonentorpedoAnzahl, int energieversorgungInProzent, int schildeInProzent,
			int huelleInProzent, int lebenserhaltungssystemeInProzent, String schiffsname, int androidenAnzahl) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
		this.energieversorgungInProzent = energieversorgungInProzent;
		this.schildeInProzent = schildeInProzent;
		this.huelleInProzent = huelleInProzent;
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
		this.schiffsname = schiffsname;
		this.androidenAnzahl = androidenAnzahl;
	}

	/**
	 * @return
	 */
	public int getPhotonentorpedoAnzahl() {
		return photonentorpedoAnzahl;
	}

	/**
	 * @param photonentorpedoAnzahl
	 */
	public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahl) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
	}

	/**
	 * @return
	 */
	public int getEnergieversorgungInProzent() {
		return energieversorgungInProzent;
	}

	/**
	 * @param energieversorgungInProzent
	 */
	public void setEnergieversorgungInProzent(int energieversorgungInProzent) {
		this.energieversorgungInProzent = energieversorgungInProzent;
	}

	/**
	 * @return
	 */
	public int getSchildeInProzent() {
		return schildeInProzent;
	}

	/**
	 * @param schildeInProzent
	 */
	public void setSchildeInProzent(int schildeInProzent) {
		this.schildeInProzent = schildeInProzent;
	}

	/**
	 * @return
	 */
	public int getHuelleInProzent() {
		return huelleInProzent;
	}

	/**
	 * @param huelleInProzent
	 */
	public void setHuelleInProzent(int huelleInProzent) {
		this.huelleInProzent = huelleInProzent;
	}

	/**
	 * @return
	 */
	public int getLebenserhaltungssystemeInProzent() {
		return lebenserhaltungssystemeInProzent;
	}

	/**
	 * @param lebenserhaltungssystemeInProznt
	 */
	public void setLebenserhaltungssystemeInProzent(int lebenserhaltungssystemeInProznt) {
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProznt;
	}

	/**
	 * @return
	 */
	public int getAndroidenAnzahl() {
		return androidenAnzahl;
	}

	/**
	 * @param androidenAnzahl
	 */
	public void setAndroidenAnzahl(int androidenAnzahl) {
		this.androidenAnzahl = androidenAnzahl;
	}

	/**
	 * @return
	 */
	public String getSchiffsname() {
		return schiffsname;
	}

	/**
	 * @param schiffsname
	 */
	public void setSchiffsname(String schiffsname) {
		this.schiffsname = schiffsname;
	}

	/**
	 * Der Methode wird ein Ladungsobjekt �bergeben. Dieses wird dem
	 * Ladungsverzeichnis hinzugef�gt.
	 * 
	 * @param neueLadung
	 */
	public void addLadung(Ladung neueLadung) {
		ladungsverzeichnis.add(neueLadung);
	}

	/**
	 * Ist die vorhandene Torpedoanzahl kleiner-gleich 0, so wird in der Konsole der
	 * String "Keine Photonentorpedos gefunden!" ausgegeben, sowie der Methode
	 * nachrichtAnAlle der String "-=*Click*=-" uebergeben. Ist die vorhandene
	 * Torpedoanzahl groe�er 0,so wird die Torpedozahl im Raumschiff um 1 reduziert,
	 * sowie der Methode "nachrichtAnAlle" der String "Photonentorpedo abgeschossen"
	 * �bergeben. Au�erdem wird die Methode treffer aufgerufen, wodurch auf das
	 * gegnerische Raumschiff geschossen wird.
	 * 
	 * @param Gegnerisches Raumschiff
	 */
	public void photonentorpedoSchiessen(Raumschiff r) {
		if (photonentorpedoAnzahl <= 0) {
			System.out.println("Keine Photonentorpedos gefunden!");
			nachrichtAnAlle("-=*Click*=-");
		} else {
			photonentorpedoAnzahl -= 1;
			nachrichtAnAlle("Photonentorpedo abgeschossen");
			treffer(r);
		}
	}

	/*
	
	 */
	/**
	 * Voraussetzung.: Ist die Energieversorgung kleiner als 50%, (Effekt.:) so wird
	 * der Methode nachrichtAnAlle der Parameter "-=*Click*=-" �bergeben.
	 * Andernfalls wird die Energieversorgung um 50% reduziert und der Methode
	 * nachrichtAnAlle der Parameter "Phaserkanone abgeschossen" gesendet. Au�erdem
	 * wird die Methode Treffer aufgerufen, wodurch das gegnerische Raumschiff
	 * angegriffen wird.
	 * 
	 * @param Gegnerisches Raumschiff
	 */
	public void phaserkanoneSchiessen(Raumschiff r) {
		if (energieversorgungInProzent < 50) {
			nachrichtAnAlle("-=*Click*=-");
		} else {
			energieversorgungInProzent -= 50;
			nachrichtAnAlle("Phaserkanone abgeschossen");
			treffer(r);
		}
	}

	/**
	 * Die Schilde des getroffenen Raumschiffs werden um 50% reduziert und die
	 * Nachricht "[Schiffsname] wurde getroffen!" wird in der Konsole ausgegeben.
	 * [Schiffsname] wird durch den Namen des getroffenen Raumschiffs ersetzt.
	 * Voraussetzung.: Sollte anschlie�end die Schilde vollstaendig zerstoert worden
	 * sein, (Effekt.:) so wird der Zustand der Huelle und der Energieversorgung
	 * jeweils um 50% reduziert. Voraussetzung.: Sollte danach der Zustand der
	 * Huelle auf 0% oder niedriger absinken, (Effekt.:) so werden die
	 * Lebenserhaltungssysteme auf 0% gesetzt und die Methode nachrichtAnAlle
	 * erhaelt den String Parameter "Alle Lebenserhaltungssysteme wurden zerst�rt".
	 * 
	 * @param Getroffene Raumschiff
	 */
	private void treffer(Raumschiff r) {
		System.out.println(r.schiffsname + " wurde getroffen!");

		schildeInProzent -= 50;
		if (schildeInProzent <= 0) {
			huelleInProzent -= 50;
			energieversorgungInProzent -= 50;
			if (huelleInProzent <= 0) {
				setLebenserhaltungssystemeInProzent(0);
				nachrichtAnAlle("Alle Lebenserhaltungssysteme wurden zerst�rt");
			}
		}
	}

	/**
	 * Der Parameter, welcher der Methode nachrichtAnAlle uebergeben wird, wird in
	 * der Konsole angezeigt, sowie dem broadcastKommunikator hinzugefuegt.
	 * 
	 * @param message
	 */
	public void nachrichtAnAlle(String message) {
		System.out.println(message);
		broadcastKommunikator.add(message);
	}

	/**
	 * Alle eingetragenen Elemente des Logbuchs werden auf der Konsole ausgegeben.
	 * 
	 * @return Gibt alle Elemente des broadcastKommunikators zurueck
	 */
	public ArrayList<String> eintraegeLogbuchZurueckgeben() {
		for (int i = 0; i < broadcastKommunikator.size(); i++) {
			System.out.println(broadcastKommunikator.get(i));
		}
		return broadcastKommunikator;
	}

	/**
	 * Voraussetzung.: Wenn sich die Ladung mit der Bezeichnung "Photonentorpedo" im
	 * Ladungsverzeichnis befindet, (Effekt.:) wird jeder Ladungseintrag mit der
	 * Bezeichnung "Photonentorpedo" gezaehlt und auf die variable countLadung mit
	 * dem Datentyp byte addiert. Voraussetzung.: Befindet sich eine Ladung mit der
	 * Bezeichnung "Photonentorpedo" im Ladungsverzeichnis und ist die Anzahl der zu
	 * ladenen Photonentorpedos groe�er-gleich die Menge der tatsaechlich
	 * vorhandenen im Ladungsobjekt, (Effekt.:) so werden alle Photonentorpedos dem
	 * Raumschiff hinzugefuegt und die Menge an Torpedos im Ladungsobjekt auf 0
	 * gesetzt. Andernfalls, wenn die zu ladene Menge kleiner der vorhandenen
	 * Torpedos im Ladungsobjekt ist, wird die Menge der Ladung um die Parameterzahl
	 * reduziert und der Photonentorpedoanzahl des Raumschiffs hinzugefuegt. Konnten
	 * in beiden Faellen Photonentorpedos eingesetzt werden, so wird die Meldung
	 * "[X] Photonentorpedo(s) eingesetzt, Vorhandene Ladungsmenge: [Y], Im
	 * Raumschiff geladen: [Z]" auf der Konsole ausgegeben. [X] wird durch die
	 * Anzahl der eingesetzten Torpedos ersetzt, [Y] der uebrig gebliebenen Menge
	 * der Ladung und [Z] durch die Anzahl der Photonentorpedos im Raumschiff.
	 * Voraussetzung.: Ist die Variable countLadungen gleich 0 wird auf der Konsole
	 * "Keine Photonentorpedos gefunden!" ausgegeben und der Methode nachrichtAnAlle
	 * der Parameter "-=*Click*=-" uebergeben.
	 * 
	 * @param Die Menge an Photonentorpedos, welche in das Raumschiff geladen werden
	 *            sollen.
	 */
	public void photonentorpedosLaden(int anzahlTorpedos) {
		byte countLadungen = 0;
		for (Ladung photonen : ladungsverzeichnis) {
			if (photonen.getBezeichnung().equals("Photonentorpedo") == true) {
				countLadungen++;
				if (anzahlTorpedos >= photonen.getMenge()) {
					int maxGeladen;
					photonentorpedoAnzahl += photonen.getMenge();
					maxGeladen = photonen.getMenge();
					photonen.setMenge(0);
					System.out.println(maxGeladen + " Photonentorpedo(s) eingesetzt" + ", Vorhandene Ladungsmenge: "
							+ photonen.getMenge() + ", Im Raumschiff geladen: " + photonentorpedoAnzahl);
				} else {
					int maxGeladen = 0;
					do {
						photonen.setMenge(photonen.getMenge() - 1);
						photonentorpedoAnzahl++;
						anzahlTorpedos--;
						maxGeladen++;
					} while (photonen.getMenge() > 0 && anzahlTorpedos > 0);
					System.out.println(maxGeladen + " Photonentorpedo(s) eingesetzt" + ", Vorhandene Ladungsmenge: "
							+ photonen.getMenge() + ", Im Raumschiff geladen: " + photonentorpedoAnzahl);
				}
			}
		}
		if (countLadungen == 0) {
			System.out.println("Keine Photonentorpedos gefunden!");
			nachrichtAnAlle("-=*Click*=-");
		}
	}

	/*
	
	 */

	/**
	 * Die Methode entscheidet anhand der uebergebenen Parameter, welche
	 * Schiffsstrukturen repariert werden sollen. Es wird eine Zufallszahl, zwischen
	 * 0 - 100 erzeugt, welche f�r die Berechnung der Reparatur ben�tigt und in der
	 * Variablen randomAnzahlDroiden mit dem Datentyp int gespeichert wird. Ist die
	 * �bergebene Anzahl von Androiden groe�er als die vorhandene Anzahl von
	 * Androiden im Raumschiff, dann wird die vorhandene Anzahl von Androiden
	 * eingesetzt. Das Ergebnis wird den auf true gesetzten Schiffsstrukturen
	 * hinzugef�gt.
	 * 
	 * @param true,  falls der User seine Schutzschilde auffuellen will
	 * @param true,  falls der User seine Energieversorgung auffuellen will
	 * @param true,  falls der User seine Huelle auffuellen will
	 * @param Anzahl an Androiden, welche zur Reparatur eingesetzt werden sollen
	 */
	public void reparaturDurchfuehren(boolean schutzschilde, boolean energieversorgung, boolean schiffshuelle,
			int anzahlDroiden) {
		boolean strukturen[] = { schutzschilde, energieversorgung, schiffshuelle };
		int reparaturBeduerftigeStrukturen = 0;
		int randomAnzahlDroiden = (int) (Math.random() * (100 - 0) + 1);
		System.out.println("Random " + randomAnzahlDroiden);
		if (anzahlDroiden > androidenAnzahl) {
			anzahlDroiden = androidenAnzahl;
		}
		System.out.println("Tats�chlich eingesetzte Droiden: " + anzahlDroiden);
		for (boolean richtig : strukturen) {
			if (richtig) {
				reparaturBeduerftigeStrukturen++;
			}
		}
		if (schutzschilde == true) {
			schildeInProzent = ((((randomAnzahlDroiden * anzahlDroiden) / reparaturBeduerftigeStrukturen)) / 100)
					+ schildeInProzent;
			if (schildeInProzent > 100) {
				setSchildeInProzent(100);
			}
		}
		if (energieversorgung == true) {
			energieversorgungInProzent = ((((randomAnzahlDroiden * anzahlDroiden) / reparaturBeduerftigeStrukturen))
					/ 100) + energieversorgungInProzent;
			if (energieversorgungInProzent > 100) {
				setEnergieversorgungInProzent(100);
			}
		}
		if (schiffshuelle == true) {
			huelleInProzent = ((((randomAnzahlDroiden * anzahlDroiden) / reparaturBeduerftigeStrukturen)) / 100)
					+ huelleInProzent;
			if (huelleInProzent > 100) {
				setHuelleInProzent(100);
			}
		}
		System.out.println("Schilde: " + schildeInProzent + ", Energy: " + energieversorgungInProzent + ", H�lle: "
				+ huelleInProzent);
	}

	/**
	 * Alle Attributwerte des Raumschiffes, werden auf der Konsole mit
	 * entsprechenden Namen ausgeben.
	 */
	public void zustandRaumschiff() {
		System.out.println("Photonentorpedoanzahl: " + getPhotonentorpedoAnzahl());
		System.out.println("Energieversorgung in Prozent: " + getEnergieversorgungInProzent());
		System.out.println("Schilde in Prozent: " + getSchildeInProzent());
		System.out.println("Huelle in Prozent: " + getHuelleInProzent());
		System.out.println("Lebenserhaltungssysteme in Prozent: " + getLebenserhaltungssystemeInProzent());
		System.out.println("Schiffsname: " + getSchiffsname());
		System.out.println("Androidenanzahl: " + getAndroidenAnzahl());
	}

	/**
	 * Alle Ladungen eines Raumschiffes werden auf der Konsole mit
	 * Ladungsbezeichnung und Menge ausgeben.
	 */
	public void ladungsverzeichnisAusgeben() {
		for (Ladung ladungen : ladungsverzeichnis) {
			System.out.println("Bezeichnung: " + ladungen.getBezeichnung() + ", Menge: " + ladungen.getMenge());
		}
	}

	/**
	 * Jedes Ladungsobjekt wird ueberprueft. Voraussetzung.: Wenn die Menge einer
	 * Ladung 0 ist, (Effekt.:) dann wird das Objekt Ladung aus der Liste entfernt.
	 */
	public void ladungsverzeichnisAufraeumen() {
		Iterator<Ladung> itr = ladungsverzeichnis.iterator();

		while (itr.hasNext()) {
			Ladung keineLadungen = itr.next();

			if (keineLadungen.getMenge() <= 0) {
				itr.remove();
			}
		}
	}
}
