public class Testing {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Ladung schneckensaft = new Ladung("Ferengi Schneckensaft", 200);
		Ladung schrott = new Ladung("Borg-Schrott", 5);
		Ladung materie = new Ladung("Rote Materie", 2);
		Ladung sonde = new Ladung("Forschungssonde", 35);
		Ladung schwert = new Ladung("Bat'leth Klingonen Schwert", 200);
		Ladung waffe = new Ladung("Plasma-Waffe", 50);
		Ladung torpedos = new Ladung("Photonentorpedo", 3);
		Raumschiff klingonen = new Raumschiff(1, 100, 100, 100, 100, "IKS Hegh'ta", 2);
		Raumschiff romulaner = new Raumschiff(2, 100, 100, 100, 100, "IRW Khazara", 2);
		Raumschiff vulkanier = new Raumschiff(0, 80, 80, 50, 100, "Ni'Var", 5);

		klingonen.addLadung(schneckensaft);
		klingonen.addLadung(schwert);
		romulaner.addLadung(schrott);
		romulaner.addLadung(materie);
		romulaner.addLadung(waffe);
		vulkanier.addLadung(sonde);
		vulkanier.addLadung(torpedos);

		klingonen.photonentorpedoSchiessen(romulaner);
		romulaner.phaserkanoneSchiessen(klingonen);
		vulkanier.nachrichtAnAlle("Gewalt ist nicht logisch");
		klingonen.zustandRaumschiff();
		vulkanier.reparaturDurchfuehren(true, true, true, 5);
		vulkanier.photonentorpedosLaden(3);
		vulkanier.ladungsverzeichnisAufraeumen();
		klingonen.photonentorpedoSchiessen(romulaner);
		klingonen.photonentorpedoSchiessen(romulaner);
		System.out.println("--------------------------------");
		klingonen.zustandRaumschiff();
		System.out.println("--------------------------------");
		romulaner.zustandRaumschiff();
		System.out.println("--------------------------------");
		vulkanier.zustandRaumschiff();
		System.out.println("--------------------------------");
		System.out.println("LADUNGEN Klingonen:");
		klingonen.ladungsverzeichnisAusgeben();
		System.out.println("LADUNGEN Romulaner:");
		romulaner.ladungsverzeichnisAusgeben();
		System.out.println("LADUNGEN Vulkanier:");
		vulkanier.ladungsverzeichnisAusgeben();
	}

}
