
/**
 * Diese Klasse modelliert eine Ladung
 * 
 * @version 1.0
 * @author Christian Pfeuffer
 *
 */
public class Ladung {
	private String bezeichnung;
	private int menge;

	/**
	 * Konstruktor fuer die Klasse Ladung
	 */
	public Ladung() {

	}

	/**
	 * Vollparametrisierter Konstruktor fuer die Klasse Ladung
	 * 
	 * @param Ladungsname
	 * @param Ladungsmenge
	 */
	public Ladung(String bezeichnung, int menge) {
		this.bezeichnung = bezeichnung;
		this.menge = menge;
	}

	/**
	 * @return
	 */
	public String getBezeichnung() {
		return bezeichnung;
	}

	/**
	 * @param bezeichnung
	 */
	public void setBezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	}

	/**
	 * @return
	 */
	public int getMenge() {
		return menge;
	}

	/**
	 * @param menge
	 */
	public void setMenge(int menge) {
		this.menge = menge;
	}
}
